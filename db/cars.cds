using { Currency, managed, sap, cuid } from '@sap/cds/common';
namespace de.bit.cars;

extend sap.common.Currencies with {
  numcode  : Integer;
  exponent : Integer; //> e.g. 2 --> 1 Dollar = 10^2 Cent
  minor    : String; //> e.g. 'Cent'
}

entity Leasing : managed, cuid {
  title  : String(111) @title: '{i18n>leasingTitle}';
  descr  : String(1111) @title: '{i18n>leasingDescr}';
}
annotate de.bit.cars.Leasing with @cds.odata.valuelist;
annotate de.bit.cars.Leasing with @UI.Identification : [{Value:title}];

entity Cars: managed, cuid {
    name: String (20) @title: '{i18n>carsName}';
    category: Association to Category;
}

entity Category : managed, cuid {
  name   : String(111) @title: '{i18n>categoryTitle}';
  co2: Integer @title: '{i18n>categoryCO2}';
  zuzahlung: Decimal @title: '{i18n>categoryZuzahlung}';
  efficiency: String @title: '{i18n>efficiency}';
  icon: String @title: '{i18n>categoryIcon}';
  displayTime: Integer;
  position: Integer;
  descr: String(50) @title: '{i18n>categoryDescr}';
  cars: Association to many Cars on cars.category = $self;
  claim: Association to many Claim on claim.category = $self;
}
annotate de.bit.cars.Category with @cds.odata.valuelist;
annotate de.bit.cars.Category with @UI.Identification : [{Value:name}];


entity Efficiency: managed {
    key ID : UUID @sap.text: 'name' @sap.label: 'ID'; 
    @readonly    
    name: String @sap.label: 'Name';
    claim: Association to many Claim on claim.efficiency = $self;
}
annotate de.bit.cars.Efficiency with @cds.odata.valuelist;
annotate de.bit.cars.Efficiency with @UI.Identification : [{Value:name}];



entity FuelType: managed {
    key ID : UUID @sap.text: 'name' @sap.label: 'ID'; 
    name: String (10) @title: '{i18n>fuelTypeName}' @sap.label: 'Name';
    chargingStation: Boolean @title: '{i18n>fuelTypeCharingStation}';
    claim: Association to many Claim on claim.fueltype = $self;
}
annotate de.bit.cars.FuelType with @cds.odata.valuelist;
annotate de.bit.cars.FuelType with @UI.Identification : [{Value:name}];


entity Users: sap.common.CodeList {
  key ID: UUID;
  email: String(50);
  firstname: String(50);
  lastname: String(50);
}

type User: Association to Users;


entity Claim: managed, cuid {
    name: String;
    leasingCompany: String;
    status : Integer enum {
      created   =  1;
      submitted =  2;
      approved  =  3;
      canceled  = -1;
    };
    @madatory
    leasing: Decimal @assert.range: [1.0, 2000.0] @title: '{i18n>claimLeasing}';
    @madatory
    maintenance: Decimal @assert.range: [1.0, 2000.0] @title: '{i18n>claimMaintenance}';
    @madatory    
    efficiency: Association to Efficiency @title: '{i18n>claimEfficiency}';
    category: Association to Category;
    fueltype: Association to FuelType;
    @madatory
    consumption: Decimal @assert.range: [1.0, 80.0] @title: '{i18n>claimConsumption}';
    @madatory
    price: Decimal @assert.range: [10000.0, 80000.0] @title: '{i18n>claimPrice}';
    currency: Currency;
    additionalCostsLeasingService: Decimal;
    additionalCostsSum: Decimal;
    chargingStation: Boolean;
    savingsConsumption: Decimal;
    user: Association to Users @title: '{i18n>username}';
    manager: Association to Users @title: '{i18n>managername}';
    @cascade: {all}
    attachment: Association to many Attachments on attachment.claim = $self;
}

entity Attachments {
    key ID: UUID;
    name: String;
    claim: Association to Claim;
    @Core.MediaType : mediaType
    file: LargeBinary;
    @Core.IsMediaType: true
    mediaType : String;
    url: String default 'file';
}

annotate Attachments with {
    file     @(
        title       : '{i18n>attachment}',
        description : '{i18n>attachment}'
    );
    mediaType @(
        title       : '{i18n>AttachmentFileType}',
        description : '{i18n>AttachmentFileType}'
    );
};

annotate Claim with { modifiedAt @odata.etag }

