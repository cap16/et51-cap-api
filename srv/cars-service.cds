using { de.bit.cars as my } from '../db/cars';
service CarService @(path:'/bit') {

    @readonly entity Leasing as SELECT from my.Leasing {ID, descr, 
        title as name
    } excluding { createdBy, modifiedBy, createdAt, modifiedAt };

    @readonly entity Category as projection on my.Category excluding { createdBy, modifiedBy, createdAt, modifiedAt };

    @readonly entity Efficiency as projection on my.Efficiency excluding { createdBy, modifiedBy, createdAt, modifiedAt };

    @readonly entity FuelType as projection on my.FuelType excluding { createdBy, modifiedBy, createdAt, modifiedAt };

    @readonly entity Cars as projection on my.Cars excluding { createdBy, modifiedBy, createdAt, modifiedAt };

    entity Claim as projection on my.Claim excluding { createdBy, modifiedBy, modifiedAt };

    entity Attachments as projection on my.Attachments excluding { createdBy, modifiedBy, createdAt, modifiedAt };
  
}