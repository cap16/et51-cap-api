const cds = require('@sap/cds')

// JWT authentication
const passport    = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt  = require('passport-jwt').ExtractJwt;
const jwksRsa     = require('jwks-rsa');
 
// load env variables
const dotenv = require('dotenv');
dotenv.config();

// To debug this module set export DEBUG=cds-oidc-jwt
const DEBUG = cds.debug('cds-oidc-jwt')

// OIDC options for JWT token
var OIDCoptions = {}
OIDCoptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
OIDCoptions.secretOrKeyProvider = jwksRsa.passportJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${process.env.OIDC_DOMAIN}/.well-known/jwks.json`
});
OIDCoptions.issuer     = process.env.OIDC_ISSUER;
OIDCoptions.audience   = process.env.OIDC_IDENTIFIER;
OIDCoptions.algorithms = process.env.OIDC_ALGORITHMS;

// check if token can be read. Return token if yes
const verify = (token, done) => {
    DEBUG && DEBUG('verifying the user')
    DEBUG && DEBUG(token, 'was the token retreived')

    // check if we have a token object and if it contains JWT specific property aud
    if (token && token.aud) {        
        return done(null, true, token);
    }
    return done(null, false, token);
}

// passport strategy
const strategy = new JwtStrategy(
    OIDCoptions,
    verify
);

// CAP user
const OIDCUser = class extends cds.User {
    is(role) {
      DEBUG && DEBUG('Requested role: ' + role)
      return role === 'any' || this._roles[role]
    }
}

// the authentication function for CAP
module.exports = (req, res, next) => {
    // set up passport
    passport.initialize();
    passport.use(strategy);
    
    // authenticate
    passport.authenticate('jwt', function (err, user, token) {
        
        if (err) {
            DEBUG && DEBUG('err')
            DEBUG && DEBUG(err)
            return next(err)
        }

        if (!user) {
            DEBUG && DEBUG('No user')
            return next(Error(token))
        }

        DEBUG && DEBUG('token')
        DEBUG && DEBUG(token)

        // map token attributes to CAP user
        var capUser = {
            id: token.sub,
            _roles: ["authenticated-user"]
        }
        req.user = new OIDCUser(capUser);

        DEBUG && DEBUG('capUser')
        DEBUG && DEBUG(capUser)

        
        console.log(req.user.is("authenticated-user"))
        console.log(req.user.is("any"))
        
        // pass on, nothing to see here
        next();

    })(req, res, next);

}